﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BehavierController : MoveController
{
    protected override void Move(Vector3 _direction)
    {
        RaycastHit2D hit = Physics2D.Raycast(transform.position, transform.up, 0.1f);
        if (hit.collider != null)
        {
            OnCollsion(hit.collider);
            //Debug.Log(hit.collider);
        }
        base.Move(_direction);
    }

    protected virtual void OnCollsion(Collider2D _col) { }
}
