﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ObjectPooling : SingletonMonoBehavier<ObjectPooling>
{
    public static Dictionary<string, List<MonoBehaviour>> dic_PoolingObject = new Dictionary<string, List<MonoBehaviour>>();

    public static Obj CreatePooling<Obj>(Obj _obj) where Obj : MonoBehaviour
    {
        List<MonoBehaviour> listObjects;
        string type = _obj.GetType().Name;

        if (!dic_PoolingObject.ContainsKey(type))
        {
            listObjects = new List<MonoBehaviour>();
            dic_PoolingObject[type] = listObjects;
            return null;
        }
        else if (dic_PoolingObject.ContainsKey(type))
        {
            listObjects = dic_PoolingObject[type];
            if (listObjects.Count > 0)
            {
                Obj obj = (Obj)listObjects[0];
                obj.gameObject.SetActive(true);
                listObjects.Remove(obj);
                return obj;
            }
            return null;
        }
        else return null;
    }

    public static void DestroyPooling<Obj>(Obj _obj) where Obj : MonoBehaviour
    {
        string type = _obj.GetType().Name;
        if (dic_PoolingObject.ContainsKey(type))
        {
            _obj.gameObject.SetActive(false);
            dic_PoolingObject[type].Add(_obj);
            return;
        }
        Destroy(_obj.gameObject);
    }
}
