﻿using UnityEngine;
using System.Collections;
using LTABase.DesignPattern;

public class TOPPICNAME
{
    public const string ENEMYDESTROYED = "EnemyDestroyed";
    public const string ENDWAVE = "EndWave";
}

public class GameManager : SingletonMonoBehavier<GameManager>
{
    //[SerializeField] Text txtCoin;
    [SerializeField] GameObject panelGameOver;
    [SerializeField] GameObject panelWinGame;

    public bool isPause;
    //int Score = 0;

    void Awake()
    {
       
        Observer.Instance.AddObserver(TOPPICNAME.ENEMYDESTROYED, Destroyed);
    }

    void Start()
    {
        //StartCoroutine(WaitTimeCreateStage(3));
    }

    IEnumerator WaitTimeCreateStage(float _s)
    {
        yield return new WaitForSeconds(_s);
       
    }

    void Destroyed(object data)
    {
      
    }

    public void GameWin()
    {
        isPause = true;
        panelWinGame.SetActive(true);
    }

    public void GameOver()
    {
        isPause = true;
        panelGameOver.SetActive(true);
    }

    void OnDestroy()
    {
        Observer.Instance.RemoveObserver(TOPPICNAME.ENEMYDESTROYED, Destroyed);
    }

    private void OnApplicationQuit()
    {
        
    }

}
