﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public abstract class ProcessingController : MonoBehaviour
{
    [SerializeField] protected int MaxValue;
    [SerializeField] protected float currentValue;
    int value;
    protected int Value
    {
        set
        {
            if (value < 0)
                this.value = 0;
            else if (value > MaxValue)
                this.value = MaxValue;
            else this.value = value;
        }
        get { return value; }
    }

    protected virtual void Update()
    {
        currentValue = Mathf.Lerp(currentValue, value, 0.75f); 
        UpdateValue(currentValue);
    }
    protected abstract void UpdateValue(float _value);
}
