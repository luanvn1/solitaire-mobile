﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MoveController : MonoBehaviour
{
    public float speed;

    protected virtual void Move(Vector3 _direction)
    {
        transform.position = Vector3.Lerp(
            transform.position,
            transform.position + _direction * speed * (Time.deltaTime * 10),
            0.1F);
    }
}
