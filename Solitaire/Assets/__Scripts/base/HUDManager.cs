﻿using UnityEngine.EventSystems;
using System.Collections.Generic;
using Luanvn;

namespace Luanvn {
    public class HUDManager : SingletonFreeAlive<HUDManager> {
        public readonly List<HUD> huds = new List<HUD>();
        public static readonly HUDComparer comparer = new HUDComparer();

        public class HUDComparer : IComparer<HUD> {
            public int Compare(HUD x, HUD y) {
                return y.Order.CompareTo(x.Order);
            }
        }

        public static int GetHudCount() {
            if(Instance == null) { return 0; }

            return Instance.huds.Count;
        }

        public static IEnumerable<HUD> GetHuds() {
            if(Instance != null) {
                foreach(HUD hud in Instance.huds) {
                    yield return hud;
                }
            }
        }

        public static void Add(HUD hud) {
            if(Instance == null) { return; }

            if(Instance.huds.Contains(hud)) { return; }

            Instance.huds.Add(hud);
            Instance.huds.Sort(comparer);
        }

        public static bool Remove(HUD hud) {
            if(!Instance) { return false; }
            return Instance.huds.Remove(hud);
        }

        public static void IgnoreInput(bool ignore) {
            EventSystem.current.currentInputModule.enabled = ignore;
            Instance.enabled = ignore;
        }

#if UNITY_ANDROID || UNITY_EDITOR
        private void Update() {
            for(int i = huds.Count - 1; i >= 0; i--) {
                if(huds[i].OnUpdate()) {
                    return;
                }
            }
        }
#endif
    }
}
