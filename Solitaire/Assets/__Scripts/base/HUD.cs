﻿using System;
using System.Collections.Generic;
using UnityEngine;

namespace Luanvn {
    [DisallowMultipleComponent]
    public abstract class HUD : MonoBehaviour {
        private List<Farm> activeFrames = new List<Farm>();
        [SerializeField] private int order = 0;
        [SerializeField] private Farm defaultFrame = null;
        [Header("[Frames]")]
        [SerializeField] private Farm[] frames = new Farm[0];

        private void LogActiveFrames() {
            int index = 1;
            foreach(Farm frame in GetActiveFrames()) {
                Debug.Log($"[{GetType()}] {index}: {frame.gameObject.name}", frame.gameObject);
                index++;
            }
        }

        protected virtual void Reset() {
            frames = GetComponentsInChildren<Farm>(true);
            if(frames.Length > 0) {
                defaultFrame = frames[0];
            }
        }
        protected virtual void OnEnable() {
            HUDManager.Add(this);
        }

        protected virtual void OnDisable() {
            HUDManager.Remove(this);
        }

        protected virtual void Start() {
            activeFrames = new List<Farm>(frames.Length);
            foreach(Farm frame in frames) { InitializeFrame(frame); }
            Show(defaultFrame);
        }

        protected virtual void OnDestroy() {
            foreach(Farm frame in frames) {
                if(frame == null) { continue; }
                frame.Remove();
                frame.OnShowed.RemoveListener(OnFrameShowed);
                frame.OnHidden.RemoveListener(OnFrameHidden);
            }
        }

        public virtual bool OnUpdate() {
            if(Input.GetKeyDown(KeyCode.Escape) && GetActiveFrameCount() > 0) {
                OnBackButtonCLicked();
                return true;
            }
            return false;
        }

        #region Get States

        public int Order { get => order; }

        public int GetFrameCount() {
            return frames.Length;
        }

        public int GetActiveFrameCount() {
            return activeFrames.Count;
        }

        public Farm GetFrameActiveOnTop() {
            if(activeFrames.Count == 0) { return null; }
            return activeFrames[activeFrames.Count - 1];
        }

        public F GetFrameActiveOnTop<F>() where F : Farm {
            return GetFrameActiveOnTop() as F;
        }

        public bool IsFrameActiveOnTop(Farm target) {
            return GetFrameActiveOnTop() == target;
        }

        public IEnumerable<Farm> GetFrames() {
            foreach(Farm frame in frames) {
                yield return frame;
            }
        }

        public IEnumerable<Farm> GetActiveFrames() {
            foreach(Farm frame in activeFrames) {
                yield return frame;
            }
        }

        public F GetFrame<F>() where F : Farm
        {
            foreach(Farm frame in GetFrames()) {
                if(frame is F) {
                    return frame as F;
                }
            }
            return null;
        }

        public F GetActiveFrame<F>() where F : Farm
        {
            foreach(Farm frame in GetActiveFrames()) {
                if(frame is F) {
                    return frame as F;
                }
            }
            return null;
        }

        public bool ContainsFrame<F>() where F : Farm
        {
            foreach(Farm frame in GetFrames()) {
                if(frame is F) {
                    return true;
                }
            }
            return false;
        }

        public bool ContainsFrame(Farm target) {
            foreach(Farm frame in GetFrames()) {
                if(frame == target) {
                    return true;
                }
            }
            return false;
        }

        public bool ContainsActiveFrame<F>() where F : Farm
        {
            foreach(Farm frame in GetActiveFrames()) {
                if(frame is F) {
                    return true;
                }
            }
            return false;
        }

        public bool ContainsActiveFrame(Farm target) {
            foreach(Farm frame in GetActiveFrames()) {
                if(frame == target) {
                    return true;
                }
            }
            return false;
        }

        #endregion

        #region Show & Hide & Pause
        public virtual Farm ShowFrameActiveOnTop(Action onCompleted = null, bool instant = false) {
            return Show(GetFrameActiveOnTop(), onCompleted, instant);
        }

        public F Show<F>(Action onCompleted = null, bool instant = false) where F : Farm
        {
            return Show(GetFrame<F>(), onCompleted, instant) as F;
        }

        public virtual Farm Show(Farm frame, Action onCompleted = null, bool instant = false) {
            if(frame == null)
                return null;

            if(!frame.Initialized) {
                InitializeFrame(frame);
            }

            if(frame.Hud != this) {
                Debug.LogWarningFormat("[HUD] The trying to open a frame {0} is not within the scope of the current hud {1}.", frame.name, GetType().Name);
                return null;
            }

            if(ContainsActiveFrame(frame)) {
                Debug.LogWarningFormat("[HUD] The trying to open a frame {0} has been opened before.", frame.name);
                activeFrames.Remove(frame);
            }

            return frame.Show(onCompleted, instant);
        }

        public F Hide<F>(Action onCompleted = null, bool instant = false) where F : Farm
        {
            return Hide(GetFrame<F>(), onCompleted, instant) as F;
        }

        public Farm HideFrameActiveTop(Action onCompleted = null, bool instant = false) {
            return Hide(GetFrameActiveOnTop(), onCompleted, instant);
        }

        public virtual Farm Hide(Farm frame, Action onCompleted = null, bool instant = false) {
            if(frame == null) { return null; }
            if(!ContainsActiveFrame(frame)) {
                Debug.LogWarningFormat("[HUD] The frame {0} has not been opened before.", frame.name);
                return null;
            }

            if(IsFrameActiveOnTop(frame)) {
                Debug.LogWarningFormat("[HUD] The closing a frame {0} is not on the top.", frame.name);
            }

            return frame.Hide(onCompleted, instant);
        }

        public virtual Farm PauseFrameActiveOnTop(Action onCompleted = null, bool instant = false) {
            return Pause(GetFrameActiveOnTop(), onCompleted, instant);
        }

        public virtual Farm Pause(Farm frame, Action onCompleted = null, bool instant = false) {
            if(frame == null)
                return null;
            if(!ContainsActiveFrame(frame)) {
                Debug.LogWarningFormat("[HUD] The frame {0} has not been opened before.", frame.name);
                return null;
            }

            return frame.Pause(onCompleted, instant);
        }

        public virtual Farm ResumeFrameActiveOnTop(Action onCompleted = null, bool instant = false) {
            return Resume(GetFrameActiveOnTop(), onCompleted, instant);
        }

        public virtual Farm Resume(Farm frame, Action onCompleted = null, bool instant = false) {
            if(frame == null)
                return null;
            if(!ContainsActiveFrame(frame)) {
                Debug.LogWarningFormat("[HUD] The frame {0} has not been opened before.", frame.name);
                return null;
            }

            return frame.Resume(onCompleted, instant);
        }


        public int HideAll() {
            int hideCount = activeFrames.Count;
            for(int i = activeFrames.Count - 1; i >= 0; i--) {
                activeFrames[i].Hide(null, true);
            }
            return hideCount;
        }

        #endregion

        protected virtual void InitializeFrame(Farm frame) {
            if(frame == null) { return; }
            frame.gameObject.SetActive(false);
            frame.Initialize(this);
            frame.OnShowed.AddListener(OnFrameShowed);
            frame.OnHidden.AddListener(OnFrameHidden);
        }

        protected virtual void OnFrameShowed(Farm frame) {
            if(frame == null) { return; }
            if(frame == GetFrameActiveOnTop()) { return; }
            if(!ContainsFrame(frame)) { return; }
            activeFrames.Add(frame);
        }

        protected virtual void OnFrameHidden(Farm frame) {
            if(frame == null) { return; }
            if(!ContainsFrame(frame)) { return; }
            bool isFrameOnTop = frame == GetFrameActiveOnTop();
            activeFrames.Remove(frame);
            if(isFrameOnTop) { GetFrameActiveOnTop()?.Resume(); }
        }

        protected virtual void OnBackButtonCLicked() {
            Farm frameOnTop = GetFrameActiveOnTop();
            if(frameOnTop == null) { return; }
            frameOnTop.OnBack();
        }
    }

    public abstract class HUD<T> : HUD where T : HUD<T> {
        private static T instance;

        public static T Instance {
            get {
                if(instance == null) {
                    instance = FindObjectOfType<T>();
                    if(instance == null) {
                       // Logs.LogError(string.Format("[SINGLETON] Không tìm thấy class {0} trong cảnh!", typeof(T)));
                    }
                }
                return instance;
            }
        }

        protected virtual void Awake() {
            if(instance == null) {
                instance = this as T;
            }
            else if(instance != this) {
              //  Logs.LogError(string.Format("[SINGLETON] Có nhiều hơn một thể hiện của class {0} trong cảnh!", typeof(T)));
                Destroy(this.gameObject);
            }
        }

        protected override void OnDestroy() {
            base.OnDestroy();
            instance = null;
        }
    }
}
