﻿using Luanvn;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class UIIngamePage : Farm
{
    [SerializeField] private ButtonBase btnPause = null;



    protected override void OnInitialize(HUD hud)
    {
        base.OnInitialize(hud);

        btnPause.AddListener(ButtonPause);

    }

    private void ButtonPause()
    {
         UIIngameManager.Instance.Show<PausePanel>();
    }

}
