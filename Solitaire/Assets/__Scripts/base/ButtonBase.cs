﻿using UnityEngine;
using System.Collections;
using UnityEngine.EventSystems;
using UnityEngine.Events;

public class ButtonBase : MonoBehaviour, IPointerDownHandler, IPointerUpHandler, IPointerClickHandler {
    [SerializeField] protected float clickScale = 0.95f;
    public bool interactable = true;
    public UnityEvent onClick;

    const float ZoomOutTime = 0.1f;
    const float ZoomInTime = 0.1f;
    Vector3 originScale = new Vector3(1.0f, 1.0f, 1.0f);

    protected void Start() {
        originScale = transform.localScale;
    }

    public virtual void SetState(bool enable) {
        interactable = enable;
    }

    public void AddListener(UnityAction action) {
        onClick.AddListener(action);
    }

    public void RemoveListener(UnityAction action) {
        onClick.RemoveListener(action);
    }

    public void RemoveAllListener() {
        onClick.RemoveAllListeners();
    }

    public void OnPointerDown(PointerEventData eventData) {
        if(interactable) {
            transform.localScale = originScale * clickScale;
        }
    }

    public void OnPointerUp(PointerEventData eventData) {
        if(interactable) {
            transform.localScale = originScale;
        }
    }

    public void OnPointerClick(PointerEventData eventData) {
        if(interactable) {
            onClick?.Invoke();
        }
    }
}
