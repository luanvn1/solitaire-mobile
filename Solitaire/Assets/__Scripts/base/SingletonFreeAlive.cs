﻿using UnityEngine;

namespace Luanvn
{
    //
    // Summary:
    //     "Instance" = new GameObject if can not find it on scene.
    //     No scene reference variables.
    //     Instance is DontDestroyOnLoad
    public class SingletonFreeAlive<T> : SingletonFree<T> where T : Component
    {
       // public SingletonFreeAlive();

        //
        // Summary:
        //     If this is set to "true", gameObject will be DontDestroyOnLoad
        //     Do not use this outside the Lib
        protected override bool KeepAlive { get; }
    }
}