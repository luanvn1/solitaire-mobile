﻿using UnityEngine.UI;
using Luanvn;
using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class PausePanel : Farm {
    [SerializeField] private ButtonBase btnYes = null;
    [SerializeField] private ButtonBase btnNo = null;
   

    protected override void OnInitialize(HUD hud) {
        base.OnInitialize(hud);
       
        btnYes.AddListener(ButtonYes);
        btnNo.AddListener(ButtonNo);
    }

    protected override void OnShow(Action onCompleted = null, bool instant = false) {
        Time.timeScale = 0;
        base.OnShow(onCompleted, instant);
    }

    private void ButtonYes() {
        Time.timeScale = 1f;
       // ManagerScene.Instance.Load(IDScene.Home);
    }

    private void ButtonNo() {
        Time.timeScale = 1f;
        Hide();
    }
}
