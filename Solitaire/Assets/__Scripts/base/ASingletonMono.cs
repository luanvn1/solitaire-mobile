﻿

using UnityEngine;

namespace Luanvn.Private
{
    //
    // Summary:
    //     Base Singleton class which is MonoBehavior. Don't use this class outside the
    //     lib
    public abstract class ASingletonMono<T> : MonoBehaviour where T : Component
    {
        //
        // Summary:
        //     This is local variable. Use 'Instance' from outsite.
        protected static T __instance;

        //protected ASingletonMono();

        //
        // Summary:
        //     If you want to check null, use this property instead of calling to "Instance"
        //     because "Instance" can auto create an emty gameobject, so "Instance" can never
        //     null
        public static bool Initialized { get; }
        //
        // Summary:
        //     If this is set to "true", gameObject will be DontDestroyOnLoad
        //     Do not use this outside the Lib
        protected virtual bool KeepAlive { get; }

        //
        // Summary:
        //     This method is empty function, just use to prepare initialize the "Instance"
        //     to improve Ram/ CPU (to preload or decompress all asset inside)
        //     Call this at the first application script (exp: BasePreload.cs)
        //     Be carefully if you override this and put your custom initialzation here, because
        //     this function can be call many times on any where, so the initialization inside
        //     will be init many times too
       // public virtual void Preload();
        //
        // Summary:
        //     This function will be call automaticaly from "Awake" only one times (on the first
        //     call of "Instance")
        //     Put your custom initialize here. No need to call "base.OnAwake"
        protected abstract void OnAwake();
        //
        // Summary:
        //     This will release instance = null. if you override this, remember to call "base.OnDestroy"
        //     at the end of this function
       // protected virtual void OnDestroy();
    }
}