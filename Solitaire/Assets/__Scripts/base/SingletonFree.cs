﻿

using Luanvn.Private;
using UnityEngine;

namespace Luanvn
{
    //
    // Summary:
    //     "Instance" = new GameObject if can not find it on scene.
    //     No scene reference variables.
    public class SingletonFree<T> : ASingletonMono<T> where T : Component
    {
        //public SingletonFree();

        //
        // Summary:
        //     If you want to check null, use 'if (T.Initialized)' instead of 'if (T.Instance
        //     != null)'
        public static T Instance { get; }

        protected override void OnAwake()
        {
            throw new System.NotImplementedException();
        }

        //
        // Summary:
        //     This function will be call automaticaly from "Awake" only one times (on the first
        //     call of "Instance")
        //     Put your custom initialize here. No need to call "base.OnAwake"
        
    }
}