﻿using UnityEngine.Events;

namespace Luanvn
{
    [System.Serializable]
    public class FrameEvent : UnityEvent<Farm> { }
}
