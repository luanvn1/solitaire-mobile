﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;


public class Bonus : SingletonMonoBehavier<Bonus>
{
    [SerializeField] GameObject bonus;
       
    public int bonusScore = 0;

    public void SetBonus(int _score)
    {
        bonusScore += 1;
        for (int i = 0; i < bonusScore; i++)
        {
            bonus.transform.GetChild(i).GetChild(0).gameObject.SetActive(true);
        }
        if(bonusScore == 5)
        {
            ResetBonus();
            //them tien
        }
    }
    public void ResetBonus()
    {
        bonusScore = 0;
        for (int i = 0; i < bonus.transform.childCount; i++)
        {
            bonus.transform.GetChild(i).GetChild(0).gameObject.SetActive(false);
        }
    }
}
