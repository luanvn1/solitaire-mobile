﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;
public class CardGold : MonoBehaviour,IPointerClickHandler
{
    Transform cardmoveUI;
    public static bool cardMove;
    public Action Func;
    public CardProspector target;
    public Transform layoutAnchor;
    public Layout layout;
    public Prospector prospector;

    void Awake()
    {
       // cardmoveUI = GameObject.FindGameObjectWithTag("cardgold").transform;
       
    }
    public void OnPointerClick(PointerEventData eventData)
    {
        Func();
    }
    void Update()
    {
        if (cardMove)
        {
            transform.position = Vector3.Lerp(transform.position, cardmoveUI.position, 0.1F);
        }
        
    }
    //void ClickCardGold()
    //{
    //    cardmoveUI.gameObject.SetActive(true);
    //}
    void MoveCardTarget(CardProspector card)
    {
     
        if (target != null)
        {
            print(target.suit + target.rank);
        }

        target = card;
        card.state = eCardState.target;
        card.transform.parent = layoutAnchor;

        card.transform.localPosition = new Vector3(
            layout.multiplier.x * layout.discardPile.x,
            layout.multiplier.y * layout.discardPile.y,
            -layout.discardPile.layerID);

        card.faceUp = true;

        card.SetSortingLayerName(layout.discardPile.layerName);
        card.SetSortOrder(0);
    }

}
