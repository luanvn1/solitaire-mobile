﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
public class Shop : MonoBehaviour
{
    [System.Serializable] class ShopItem
    {
        public Sprite image, image1;
        public int Price;
        public bool isPurchased = false;
    }
    [SerializeField] List<ShopItem> shopItems;

    GameObject item;
    GameObject g;
    [SerializeField] Transform ShopScollView;
    Button buyBtn;
    public Button btnClose;
    // Start is called before the first frame update
    void Start()
    {
       // int len = shopItems.Count;
        item = ShopScollView.GetChild(0).gameObject;
        for(int i =0; i< 6; i++)
        {
            g = Instantiate(item, ShopScollView);
            g.transform.GetChild(0).GetComponent<Image>().sprite = shopItems[i].image;
            g.transform.GetChild(0).GetComponent<Image>().sprite = shopItems[i].image1;
            g.transform.GetChild(1).GetChild(0).GetComponent<Text>().text = shopItems[i].Price.ToString();
            buyBtn = g.transform.GetChild(3).GetComponent<Button>();
            buyBtn.interactable = !shopItems[i].isPurchased;
            buyBtn.AddEvenListener(i, OnshopItemClick);
            //g.transform.GetChild(2).GetComponent<Button>().interactable = !shopItems[i].isPurchased;
        }
        Destroy(ShopScollView);
    }
    void OnshopItemClick(int itemIndex)
    {
        Debug.Log(itemIndex);

        shopItems[itemIndex].isPurchased = true;
        buyBtn = ShopScollView.GetChild(itemIndex).GetChild(2).GetComponent<Button>();
        buyBtn.interactable = false;
        buyBtn.transform.GetChild(0).GetComponent<Text>().text = "PURCHASED";
    }

    public void OpenShop()
    {
        gameObject.SetActive(true);   
    }
    public void OnClose()
    {
        gameObject.SetActive(false);
    }
}
